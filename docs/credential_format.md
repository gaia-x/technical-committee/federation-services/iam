# Credential Format

This section extends the information that the following documents provide about **Gaia-X Credentials**:

- the [Gaia-X Architecture Document](https://docs.gaia-x.eu/technical-committee/architecture-document/), which
  - defines the Conceptual Model of entities that can have Gaia-X Credentials (**ServiceOffering**s, their **Provider**s, and the **Resource**s they aggregate), and
  - introduces Gaia-X Credentials and their lifecycle on a high level
- the [Gaia-X Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.04-prerelease/),
  which defines the Gaia-X conformity assessment schemes and the requirements for the respective Trust Anchors.
- the [Gaia-X Ontology](https://gaia-x.gitlab.io/technical-committee/service-characteristics-working-group/service-characteristics/ontology/gaia-x/)
containing the models to automate the Gaia-X Compliance.

## Gaia-X Credential Format

A **Gaia-X Credential** is a Verifiable Credential (VC)
following [W3C Verifiable Credential Data Model 2.0](https://www.w3.org/TR/vc-data-model-2.0/) using the Gaia-X Ontology
which is available via the Gaia-X Registry.
A holder can put several Gaia-X credentials together to build
a [Verifiable Presentation (VP)](https://www.w3.org/TR/vc-data-model-2.0/#presentations).

A Verifiable Presentation contains one or more Verifiable Credentials with individual disclosed claims and packaged
in such a way that the authorship of the data is verifiable. It SHOULD be extremely short-lived, and bound to a
challenge provided by a verifier.
Each Verifiable Credential that might have been issued by multiple issuers contains signed **claims** about one or more
*subjects*.

This section extends the [W3C Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) to
specify how it shall be applied in the scope of Gaia-X.

### Gaia-X Credential Example

This section extends the [W3C Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) to
specify how it shall be applied in the scope of Gaia-X.

The following listing shows an example of a Gaia-X Credential document before becoming a Verifiable Credential.

<details>
<summary>Example Verifiable Credential</summary>

```json linenums="1" title="document.json"
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "@type": [
    "VerifiableCredential",
    "LegalPerson"
  ],
  "@id": "https://example.org/legal-participant/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
  "issuer": "did:web:example.org",
  "validFrom": "2024-01-01T12:26:22.601516+00:00",
  "validUntil": "2024-04-01T12:26:22.601516+00:00",
  "credentialSubject": {
    "id": "https://example.org/legal-participant-json/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
    "type": "gx:LegalPerson",
    "gx:legalName": "Example Org",
    "gx:legalRegistrationNumber": {
      "id": "https://example.org/gaiax-legal-registration-number/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json"
    },
    "gx:headquarterAddress": {
      "gx:countrySubdivisionCode": "FR-75"
    },
    "gx:legalAddress": {
      "gx:countrySubdivisionCode": "FR-75"
    }
  }
}
```

Once it has been encoded into a Verifiable Credential using
the [VC-JWT specification](https://www.w3.org/TR/vc-jose-cose/#securing-json-ld-verifiable-credentials-with-jose)
it will become the following Verifiable Credential.

```
eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmcjSldLMjAyMC1SU0EifQ.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MiJdLCJAdHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkxlZ2FsUGFydGljaXBhbnQiXSwiQGlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9sZWdhbC1wYXJ0aWNpcGFudC82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsImlzc3VlciI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmciLCJ2YWxpZEZyb20iOiIyMDI0LTA0LTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsInZhbGlkVW50aWwiOiIyMDI0LTAxLTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7IkBjb250ZXh0IjpbImh0dHBzOi8vcmVnaXN0cnkubGFiLmdhaWEteC5ldS92MS9hcGkvdHJ1c3RlZC1zaGFwZS1yZWdpc3RyeS92MS9zaGFwZXMvanNvbmxkL3RydXN0ZnJhbWV3b3JrIyJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWwtcGFydGljaXBhbnQtanNvbi82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsInR5cGUiOiJneDpMZWdhbFBhcnRpY2lwYW50IiwiZ3g6bGVnYWxOYW1lIjoiRXhhbXBsZSBPcmciLCJneDpsZWdhbFJlZ2lzdHJhdGlvbk51bWJlciI6eyJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvZ2FpYXgtbGVnYWwtcmVnaXN0cmF0aW9uLW51bWJlci82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiJ9LCJneDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiZ3g6Y291bnRyeVN1YmRpdmlzaW9uQ29kZSI6IkZSLTc1In0sImd4OmxlZ2FsQWRkcmVzcyI6eyJneDpjb3VudHJ5U3ViZGl2aXNpb25Db2RlIjoiRlItNzUifX19.NxVb_3t8WE0XWelPZsaKAcME8E28Vi5H0utVvJeYCr6cGKfj9Snl2C7buSpJIz-ZoPAKQJLKK1gWHsMh5Ge1I99vhZZ61vsGBfjLO0gFhLBwpriLMW7YkJnKD4QoTv-RxBX3JCakUCE_vkSceUOeRUfJKfEEfbyAAMjBnRZsbeH7xt5MLrs482TxYx2HhSdNkxVZU4UHK0hGSauoGfZrHV5e7XT4N2q4vXIRfN3iihYbw4-27sSDgNwOkuY34lWwRZSQsP3PoBneJcH0KDvEPgKvOt8V9ZM78wbyH9NIae8qAEKwVNF61cs3XQx6-0bqI6h0n9I4C93ShXxrqmjgTA
```

This VC-JWT can be analyzed and verified with tools such
as [JWT.io's debugger](https://jwt.io/#debugger-io?token=eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmcjSldLMjAyMC1SU0EifQ.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MiJdLCJAdHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkxlZ2FsUGFydGljaXBhbnQiXSwiQGlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9sZWdhbC1wYXJ0aWNpcGFudC82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsImlzc3VlciI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmciLCJ2YWxpZEZyb20iOiIyMDI0LTA0LTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsInZhbGlkVW50aWwiOiIyMDI0LTAxLTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7IkBjb250ZXh0IjpbImh0dHBzOi8vcmVnaXN0cnkubGFiLmdhaWEteC5ldS92MS9hcGkvdHJ1c3RlZC1zaGFwZS1yZWdpc3RyeS92MS9zaGFwZXMvanNvbmxkL3RydXN0ZnJhbWV3b3JrIyJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWwtcGFydGljaXBhbnQtanNvbi82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsInR5cGUiOiJneDpMZWdhbFBhcnRpY2lwYW50IiwiZ3g6bGVnYWxOYW1lIjoiRXhhbXBsZSBPcmciLCJneDpsZWdhbFJlZ2lzdHJhdGlvbk51bWJlciI6eyJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvZ2FpYXgtbGVnYWwtcmVnaXN0cmF0aW9uLW51bWJlci82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiJ9LCJneDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiZ3g6Y291bnRyeVN1YmRpdmlzaW9uQ29kZSI6IkZSLTc1In0sImd4OmxlZ2FsQWRkcmVzcyI6eyJneDpjb3VudHJ5U3ViZGl2aXNpb25Db2RlIjoiRlItNzUifX19.NxVb_3t8WE0XWelPZsaKAcME8E28Vi5H0utVvJeYCr6cGKfj9Snl2C7buSpJIz-ZoPAKQJLKK1gWHsMh5Ge1I99vhZZ61vsGBfjLO0gFhLBwpriLMW7YkJnKD4QoTv-RxBX3JCakUCE_vkSceUOeRUfJKfEEfbyAAMjBnRZsbeH7xt5MLrs482TxYx2HhSdNkxVZU4UHK0hGSauoGfZrHV5e7XT4N2q4vXIRfN3iihYbw4-27sSDgNwOkuY34lWwRZSQsP3PoBneJcH0KDvEPgKvOt8V9ZM78wbyH9NIae8qAEKwVNF61cs3XQx6-0bqI6h0n9I4C93ShXxrqmjgTA).

The following private and public key have been used to sign this VC-JWT example and **all the examples in the upcoming
chapters**.

This key pair will have the following ID : `did:web:example.org#JWK-RSA`. You will notice it in the
`kid` header claim of the example JWT below.

```
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC7VJTUt9Us8cKj
MzEfYyjiWA4R4/M2bS1GB4t7NXp98C3SC6dVMvDuictGeurT8jNbvJZHtCSuYEvu
NMoSfm76oqFvAp8Gy0iz5sxjZmSnXyCdPEovGhLa0VzMaQ8s+CLOyS56YyCFGeJZ
qgtzJ6GR3eqoYSW9b9UMvkBpZODSctWSNGj3P7jRFDO5VoTwCQAWbFnOjDfH5Ulg
p2PKSQnSJP3AJLQNFNe7br1XbrhV//eO+t51mIpGSDCUv3E0DDFcWDTH9cXDTTlR
ZVEiR2BwpZOOkE/Z0/BVnhZYL71oZV34bKfWjQIt6V/isSMahdsAASACp4ZTGtwi
VuNd9tybAgMBAAECggEBAKTmjaS6tkK8BlPXClTQ2vpz/N6uxDeS35mXpqasqskV
laAidgg/sWqpjXDbXr93otIMLlWsM+X0CqMDgSXKejLS2jx4GDjI1ZTXg++0AMJ8
sJ74pWzVDOfmCEQ/7wXs3+cbnXhKriO8Z036q92Qc1+N87SI38nkGa0ABH9CN83H
mQqt4fB7UdHzuIRe/me2PGhIq5ZBzj6h3BpoPGzEP+x3l9YmK8t/1cN0pqI+dQwY
dgfGjackLu/2qH80MCF7IyQaseZUOJyKrCLtSD/Iixv/hzDEUPfOCjFDgTpzf3cw
ta8+oE4wHCo1iI1/4TlPkwmXx4qSXtmw4aQPz7IDQvECgYEA8KNThCO2gsC2I9PQ
DM/8Cw0O983WCDY+oi+7JPiNAJwv5DYBqEZB1QYdj06YD16XlC/HAZMsMku1na2T
N0driwenQQWzoev3g2S7gRDoS/FCJSI3jJ+kjgtaA7Qmzlgk1TxODN+G1H91HW7t
0l7VnL27IWyYo2qRRK3jzxqUiPUCgYEAx0oQs2reBQGMVZnApD1jeq7n4MvNLcPv
t8b/eU9iUv6Y4Mj0Suo/AU8lYZXm8ubbqAlwz2VSVunD2tOplHyMUrtCtObAfVDU
AhCndKaA9gApgfb3xw1IKbuQ1u4IF1FJl3VtumfQn//LiH1B3rXhcdyo3/vIttEk
48RakUKClU8CgYEAzV7W3COOlDDcQd935DdtKBFRAPRPAlspQUnzMi5eSHMD/ISL
DY5IiQHbIH83D4bvXq0X7qQoSBSNP7Dvv3HYuqMhf0DaegrlBuJllFVVq9qPVRnK
xt1Il2HgxOBvbhOT+9in1BzA+YJ99UzC85O0Qz06A+CmtHEy4aZ2kj5hHjECgYEA
mNS4+A8Fkss8Js1RieK2LniBxMgmYml3pfVLKGnzmng7H2+cwPLhPIzIuwytXywh
2bzbsYEfYx3EoEVgMEpPhoarQnYPukrJO4gwE2o5Te6T5mJSZGlQJQj9q4ZB2Dfz
et6INsK0oG8XVGXSpQvQh3RUYekCZQkBBFcpqWpbIEsCgYAnM3DQf3FJoSnXaMhr
VBIovic5l0xFkEHskAjFTevO86Fsz1C2aSeRKSqGFoOQ0tmJzBEs1R6KqnHInicD
TQrKhArgLXX4v3CddjfTRJkFWDbE/CkvKZNOrcf1nhaGCPspRJj2KUkj1Fhl9Cnc
dn/RsYEONbwQSjIfMPkvxF+8HQ==
-----END PRIVATE KEY-----

-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo
4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u
+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh
kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ
0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg
cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc
mwIDAQAB
-----END PUBLIC KEY-----
```

</details>

## Digital Signature Standard

This document follows the [FIPS 186-5](https://csrc.nist.gov/publications/detail/fips/186/5/final) standard on Digital
Signature Standard (DSS).

## Decentralized Identifiers

This section extends the [W3C Decentralized Identifiers](https://www.w3.org/TR/did-core/) to specify how it shall be
applied in the scope of Gaia-X.

### Verification Methods

To ensure a Gaia-X Credential's integrity and authenticity, its claims MUST be cryptographically signed by the
Participant that is issuing them.  
This is done to avoid tampering and to technically allow to check the origin of the claims.

The supported verification methods are described below.

#### JSON Web Key

This section extends the specification from
the [W3C JSON Web Key](https://w3c.github.io/vc-data-integrity/vocab/security/vocabulary.html#JsonWebKey).

A Verifiable Credential is *Gaia-X Conformant* if:

- it is signed by a trusted issuer present in the Gaia-X Registry
- its issuer has a verifiable identity coming from one of
  the [Trust Anchors](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/23.10/Gaia-X_Trust_Anchors/)
- it complies with the Gaia-X Ontology Shacl Shapes
- it uses the enveloping proof encoding specified by this document

Without a means to link the issuer's verification method to a Gaia-X Trust Anchor, the Gaia-X Compliance verification
will fail.

To be able to assess the chain of trust, the `publicKeyJwk` property MUST include either
the [RFC7517 `x5c`](https://www.rfc-editor.org/rfc/rfc7517#section-4.7) (X.509 Certificate Chain) parameter
or [RFC7517 `x5u`](https://www.rfc-editor.org/rfc/rfc7517#section-4.6) (X.509 URL) parameter.  
The `x5u` parameter should be resolvable to a `X509` `.crt`, `.pem`, `.der` or `.p7b` file which contains a valid Gaia-X
Trust Anchor eligible for the signed claims.

To ensure the correct cryptographic tools are used with the public key, the `alg` property MUST be specified, the value must comply with the JSON Web Algorithms [RFC7518 `alg`](https://www.rfc-editor.org/rfc/rfc7518#section-3.1)

<details>
<summary>Example of verificationMethod</summary>

```json title="verifiable_method.json"
{
  "@context": [
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/jwk/v1"
  ],
  "id": "did:web:example.org",
  "verificationMethod": [
    {
      "id": "did:web:example.org#JWK-RSA",
      "type": "JsonWebKey",
      "controller": "did:web:example.org",
      "publicKeyJwk": {
        "kty": "RSA",
        "n": "0oxjNiK1D5lcowRFjpzY8AY8DwkH5I4mXnz9f4ILcFIV8HG3EyWnAkYgf5EJO91P7t4NHESxuNvSXSSYe5UBizWXNfmKClX2l3g0-6Iw0amxtdgrAmX-HxOprdxfBMt1xRwf6B4M9CQzGBIDAMW5B8-zJsbnGPIz0iWJ2qdFvtPtD3He3ds7azrcLmEaQLqg2yw7Fw5xwmSRodYasXIOhk1Wg4lqiJp2bG9JBaWdJW7Q2kee39UxnAXCQKmflkQuPAALxj5C-5436n0--64Xd6JH6QeaKgyQPGLEEVwvNibiZD8PcbXqyCDCNBD7DmTOTLfjz03qH5qjYvpuo5K6Aw",
        "e": "AQAB",
        "kid": "q44a8UEJIUNs43nWyLSCxNQXQRB40ccilgRqsZ7n0Og",
        "alg": "PS256",
        "x5u": "https://example.org/.well-known/chain.pem"
      }
    }
  ],
  "authentication": [
    "did:web:example.org#JWK-RSA"
  ],
  "assertionMethod": [
    "did:web:example.org#JWK-RSA"
  ]
}
```

</details>

## Use of Identifiers in Gaia-X Credentials

Each of the following MUST have a different identifier:

* a Verifiable Presentation
* a Verifiable Credential inside a Verifiable Presentation
* the subject of a Verifiable Credential, i.e., the Conceptual Model entity that claims are made about.

Gaia-X Credentials MAY reference other Gaia-X Credentials. Consider, for example, a *ServiceOffering* that:

* is provided by a *Provider*,
* is a composition of other *ServiceOffering*s, or
* is an aggregation of *Resources*.

## Verifiable Credential and Verifiable Presentation

This section extends the [W3C Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) to
specify how it shall be applied in the scope of Gaia-X.

### Namespace Bindings and Contexts

On the level of the Verifiable Presentation and the Verifiable Credentials contained in the Verifiable Presentation, a
Gaia-X Credential MUST adhere to the vocabulary of the Verifiable Credentials Data Model, i.e., use terms from
the `https://www.w3.org/2018/credentials#` namespace.

To enable human authors of Gaia-X Credentials to write down these terms conveniently, they MAY, by using the `@context`
keyword on the level of the Verifiable Presentation, e.g.:

- reference the [JSON-LD context](https://www.w3.org/TR/json-ld11/#the-context) provided by the Verifiable Credentials
  Data Model (https://www.w3.org/ns/credentials/v2) like in the initial example listing, or
- define their own context, which
  - defines the above namespace as the default vocabulary using the `@vocab` keyword, or
  - maps the above namespace to a designated prefix, e.g., `"cred"`.

Similarly, the claims about any credential subject MUST adhere to the vocabulary of the Gaia-X Credential Schemas
published in the [Gaia-X Registry](https://registry.gaia-x.eu/), or to Federation-specific specializations of this
vocabulary.

### Identifiers

The `@id` MUST be present and unique for a given `issuer`.

The `@id` keyword is aliased to `id`. Consequently we MAY also use this alias.

It is up to the `issuer` to decide if the `@id` is a resolvable [URL](https://url.spec.whatwg.org/) or not.


### Integrity of Related Resources

In order to enable reference to objects - Verifiable Credentials or credential subject - which are not under control of
the same issuers, it is RECOMMENDED to specify an `@sri` [Subresource Integrity](https://www.w3.org/TR/SRI/) attribute
to enable the verification of the integrity of the referenced object.

The `sri` attribute is computed by taking the hash of the referenced normalized JSON object.  
The JSON object is normalized following the JSON Canonicalization Scheme (JCS) defined in
the [RFC 8785](https://www.rfc-editor.org/rfc/rfc8785).

<details>
<summary>Example of `sri` attribute</summary>

```json title="SRI attribute"
{
  "@id": "https://example.com/ABC",
  "sri": "sha256-b9a822666c3569a8ae80c897a1984f68bbdffa1f8141cacdb3f168b1c0b9aa36"
}
```

</details>

### Types

The `@type` property MUST be present in Verifiable Presentation, Verifiable Credentials, and Credentials.
The expected values for the first `@type` property are:

- `"VerifiablePresentation"` for a Verifiable Presentation
- `"EnvelopedVerifiablePresentation"` for an Enveloped Verifiable Presentation encoded as a VC-JWT
- `"VerifiableCredential"` for a Verifiable Credential
- `"EnvelopedVerifiableCredential"` for an Enveloped Verifiable Credential encoded as a VC-JWT

This `@type` can be followed with one or more credential related types (
ie. `@type: ['Verifiable Credential', 'LegalPerson']`).

The `@type` keyword is aliased to `type`. Consequently, we MAY also use this alias.

The expected values for the `@type` property of a credential subject are given by the taxonomy of classes defined in
the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/), having the
superclasses `Participant`, `ServiceOffering` and `Resource`.

A Federation MAY define additional subclasses of these by further shapes hosted in its Catalogue(s).
In the future, Gaia-X and federations MAY also define additional, more specific credential types.

#### Schema Validation

A Schema for Gaia-X Credentials, to be used as the vocabulary of the claims about credential subjects, MUST be available
in the form of SHACL shapes (cf. the [W3C Shapes Constraint Language SHACL](https://www.w3.org/TR/shacl/)) in the Gaia-X
Registry or in the Catalogue of a Federation.  
At any point where Gaia-X Credentials are created or received, a certain set of SHACL shapes is known, which forms a
*shapes graph*.
A Gaia-X Credential forms a *data graph*. For compliance with Gaia-X and/or a specific Federation, this *data graph* MUST
be validated against the given *shapes
graph* according to the [SHACL specification](https://www.w3.org/TR/shacl/#validation-definition).

### Issuers

The `issuer` property MUST be present in Verifiable Credential and Verifiable Presentation. The value of the `issuer`
property must be a resolvable URI.

The supported schemes for `issuer`'s URI are:

- `https`
- `did`. The supported [DID methods](https://w3c.github.io/did-spec-registries/#did-methods) are:
  - `web`

### validFrom

The [`validFrom`](https://www.w3.org/TR/vc-data-model-2.0/#validity-period) property is MANDATORY for Verifiable Credential and
Verifiable Presentation.

### validUntil

The [`validUntil`](https://www.w3.org/TR/vc-data-model-2.0/#validity-period) property is RECOMMENDED for Verifiable Credential
and Verifiable Presentation.

### Verifiable Credential

Verifiable Credentials are encoded as [Json Web Tokens](https://datatracker.ietf.org/doc/html/rfc7519) as described in
the [VC-JWT specification](https://www.w3.org/TR/vc-jose-cose/). This type of proofing is
an [enveloping proof](https://www.w3.org/TR/vc-data-model-2.0/#securing-mechanisms).

A JWT consists in a header, a payload and a signature each element being separated by a dot (`.`).

<details>
<summary>Example Verifiable Credential</summary>

If we use the following credential:

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "@type": [
    "VerifiableCredential",
    "LegalParticipant"
  ],
  "@id": "https://example.org/legal-participant/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
  "issuer": "did:web:example.org",
  "validFrom": "2024-04-01T12:26:22.601516+00:00",
  "validUntil": "2024-01-01T12:26:22.601516+00:00",
  "credentialSubject": {
    "id": "https://example.org/legal-participant-json/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
    "type": "gx:LegalPerson",
    "gx:legalName": "Example Org",
    "gx:legalRegistrationNumber": {
      "id": "https://example.org/gaiax-legal-registration-number/68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json"
    },
    "gx:headquarterAddress": {
      "gx:countrySubdivisionCode": "FR-75"
    },
    "gx:legalAddress": {
      "gx:countrySubdivisionCode": "FR-75"
    }
  }
}
```

The VC-JWT representation as a Verifiable Credential would be:

```
eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmcjSldLMjAyMC1SU0EifQ.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MiJdLCJAdHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkxlZ2FsUGFydGljaXBhbnQiXSwiQGlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9sZWdhbC1wYXJ0aWNpcGFudC82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsImlzc3VlciI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmciLCJ2YWxpZEZyb20iOiIyMDI0LTA0LTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsInZhbGlkVW50aWwiOiIyMDI0LTAxLTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7IkBjb250ZXh0IjpbImh0dHBzOi8vcmVnaXN0cnkubGFiLmdhaWEteC5ldS92MS9hcGkvdHJ1c3RlZC1zaGFwZS1yZWdpc3RyeS92MS9zaGFwZXMvanNvbmxkL3RydXN0ZnJhbWV3b3JrIyJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWwtcGFydGljaXBhbnQtanNvbi82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsInR5cGUiOiJneDpMZWdhbFBhcnRpY2lwYW50IiwiZ3g6bGVnYWxOYW1lIjoiRXhhbXBsZSBPcmciLCJneDpsZWdhbFJlZ2lzdHJhdGlvbk51bWJlciI6eyJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvZ2FpYXgtbGVnYWwtcmVnaXN0cmF0aW9uLW51bWJlci82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiJ9LCJneDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiZ3g6Y291bnRyeVN1YmRpdmlzaW9uQ29kZSI6IkZSLTc1In0sImd4OmxlZ2FsQWRkcmVzcyI6eyJneDpjb3VudHJ5U3ViZGl2aXNpb25Db2RlIjoiRlItNzUifX19.NxVb_3t8WE0XWelPZsaKAcME8E28Vi5H0utVvJeYCr6cGKfj9Snl2C7buSpJIz-ZoPAKQJLKK1gWHsMh5Ge1I99vhZZ61vsGBfjLO0gFhLBwpriLMW7YkJnKD4QoTv-RxBX3JCakUCE_vkSceUOeRUfJKfEEfbyAAMjBnRZsbeH7xt5MLrs482TxYx2HhSdNkxVZU4UHK0hGSauoGfZrHV5e7XT4N2q4vXIRfN3iihYbw4-27sSDgNwOkuY34lWwRZSQsP3PoBneJcH0KDvEPgKvOt8V9ZM78wbyH9NIae8qAEKwVNF61cs3XQx6-0bqI6h0n9I4C93ShXxrqmjgTA
```

To view the header, payload and signature of this example on
JWT.io [click here](https://jwt.io/#debugger-io?token=eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmcjSldLMjAyMC1SU0EifQ.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MiJdLCJAdHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkxlZ2FsUGFydGljaXBhbnQiXSwiQGlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9sZWdhbC1wYXJ0aWNpcGFudC82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsImlzc3VlciI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmciLCJ2YWxpZEZyb20iOiIyMDI0LTA0LTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsInZhbGlkVW50aWwiOiIyMDI0LTAxLTAxVDEyOjI2OjIyLjYwMTUxNiswMDowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7IkBjb250ZXh0IjpbImh0dHBzOi8vcmVnaXN0cnkubGFiLmdhaWEteC5ldS92MS9hcGkvdHJ1c3RlZC1zaGFwZS1yZWdpc3RyeS92MS9zaGFwZXMvanNvbmxkL3RydXN0ZnJhbWV3b3JrIyJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWwtcGFydGljaXBhbnQtanNvbi82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiIsInR5cGUiOiJneDpMZWdhbFBhcnRpY2lwYW50IiwiZ3g6bGVnYWxOYW1lIjoiRXhhbXBsZSBPcmciLCJneDpsZWdhbFJlZ2lzdHJhdGlvbk51bWJlciI6eyJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvZ2FpYXgtbGVnYWwtcmVnaXN0cmF0aW9uLW51bWJlci82OGE1YmJlYTk1MThlN2UyYWMxY2M3NWJjYzg4MTlhN2VkZDVjNDcxMWUwNzNmZmE0YmIyNjAwMzRkYzY0MjNjL2RhdGEuanNvbiJ9LCJneDpoZWFkcXVhcnRlckFkZHJlc3MiOnsiZ3g6Y291bnRyeVN1YmRpdmlzaW9uQ29kZSI6IkZSLTc1In0sImd4OmxlZ2FsQWRkcmVzcyI6eyJneDpjb3VudHJ5U3ViZGl2aXNpb25Db2RlIjoiRlItNzUifX19.NxVb_3t8WE0XWelPZsaKAcME8E28Vi5H0utVvJeYCr6cGKfj9Snl2C7buSpJIz-ZoPAKQJLKK1gWHsMh5Ge1I99vhZZ61vsGBfjLO0gFhLBwpriLMW7YkJnKD4QoTv-RxBX3JCakUCE_vkSceUOeRUfJKfEEfbyAAMjBnRZsbeH7xt5MLrs482TxYx2HhSdNkxVZU4UHK0hGSauoGfZrHV5e7XT4N2q4vXIRfN3iihYbw4-27sSDgNwOkuY34lWwRZSQsP3PoBneJcH0KDvEPgKvOt8V9ZM78wbyH9NIae8qAEKwVNF61cs3XQx6-0bqI6h0n9I4C93ShXxrqmjgTA).

</details>

#### Header

The VC-JWT header MUST contain the following fields:

- `alg`, the signature algorithm (ie. `PS256`)
- `typ`, the media type of the JWT which must be set to `vc+ld+json+jwt`
- `cty`, the content type of the payload which must be set to `vc+ld+json`
- `kid`, the `did:web` or URL reference to the [verification method in a DID document](#json-web-key)

Additional headers that aren't described in
the [VC-JWT](https://www.w3.org/TR/vc-jose-cose/), [JWT](https://datatracker.ietf.org/doc/html/rfc7519)
or [JWS](https://datatracker.ietf.org/doc/html/rfc7515) specifications should be ignored.

#### Payload

The payload of the VC-JWT is a standard verifiable credential with claims as described in
the [Verifiable Credential Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) specification.

Some payload claims from the [JWT specification](https://datatracker.ietf.org/doc/html/rfc7519#section-4.1) MUST be
replaced by the described verifiable credential fields such as:

- `iss` will be replaced by the verifiable credential's `issuer`
- `jti` will be replaced by the verifiable credential's `id` or `@id`
- `sub` will be replaced by the verifiable credential's `credentialSubject.id` or `credentialSubject.@id`

The `vc` and `vp` payload claims MUST NOT be present.

> ℹ️ The `iat` and `exp` payload claims represent the JWT's signature validity period whereas the `validFrom` and
> the `validUntil`
> verifiable credential payload claims represent the verifiable credential's
> data [validity period](https://www.w3.org/TR/vc-data-model-2.0/#validity-period).
> Therefore these claims can cohabit in the payload.

If the `@type` is "VerifiableCredential", the property `credentialSubject` MUST be defined. The value
of `credentialSubject` can be a Credential or an array of Credentials. A Verifiable Credential MUST have :

- an [`@id`](#identifiers),
- an [`issuer`](#issuers),
- a [`@type`](#types), and
- a [`credentialSubject`](#credential-subject) object or a [`credentialSubject`](#credential-subject) array.

> NB: The `@id` and `@type` keywords are aliased to `id` and `type` respectively. Consequently, we MAY also use these
> aliases.

#### Signature

The last element of a VC-JWT is the signature which is cryptographically secured to ensure integrity hence making the
Verifiable Credential tamper-proof.

A JWS is signed using the issuer's private key and can be verified by using the issuer's public key which is obtainable
through the issuer's DID document (referenced in the `kid` JWS header).

VC-JWT signatures are created by following the [JSON Web Signature (JWS)](https://datatracker.ietf.org/doc/html/rfc7515)
specification. [Many libraries are available online](https://jwt.io/libraries) to manage JWS creation.

#### Credential Subject

The `credentialSubject` can be an object or array of objects, containing claims.

The claims about one Gaia-X entity MAY be spread over multiple Credentials and their subjects.

Each credential subject MUST have an [`@id`](#identifiers).

A credential subject MAY be described *by value*, i.e., by stating one or more claims about it in place.
In this case, it MUST have a [`@type`](#types) as specified below.

Alternatively, a credential subject MAY be described *by reference*.
In this case, the `@id` MUST be resolvable to an RDF resource that has the same `@id`, a `@type`, and one or more
claims. See [Identifiers](#identifiers) section for more details.

The value of the `@type` property dictates the vocabulary available in
the [Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/) document for the definition of
claims about the credential subject. E.g., `LegalPerson`, `ServiceOffering`, `DataResource`, ...

<details>
<summary>Example of credentialSubject</summary>

```json
{
  "@id": "https://example.com/legalPersonABC?vcid=c93b5075b3988eda4a529afce7e7c127f607b55dc08bb12e8c9adc9e33fe814f",
  "@type": "gx:legalPerson",
  "gx:legalName": "Legal Person ABC",
  "gx:legalRegistrationNumber": {
    "@id": "https://gaia-x.eu/legalRegistrationNumber_VC.json"
  },
  "gx:headquarterAddress": {
    "gx:countrySubdivisionCode": "FR-IDF"
  },
  "gx:legalAddress": {
    "gx:countrySubdivisionCode": "FR-IDF"
  }
}
```

</details>

### Enveloped Verifiable Credential

An Enveloped Verifiable Credential is a convenient way of describing a Verifiable Credential that has been encoded with
an enveloping proof such as VC-JWT.

It's represented as a basic JSON object with three fields:

- `@context` which is usually set to `https://www.w3.org/ns/credentials/v2`
- `id` containing the data of the VC-JWT in the form of
  an `application/vc+ld+json+jwt` [data URL](https://www.rfc-editor.org/rfc/rfc2397)
- `type` which must be set to `EnvelopedVerifiableCredential`

This type of Verifiable Credential is very useful in the context of
a [Verifiable Presentation](#verifiable-presentation)
to embed multiple Verifiable Credentials.

<details>
<summary>Example Enveloped Verifiable Credential</summary>

Below is an example representing the [Verifiable Credential example](#verifiable-credential) as an Enveloped Verifiable
Credential.

```json
{
  "@context": "https://www.w3.org/ns/credentials/v2",
  "id": "data:application/vc+ld+json+jwt;eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmc6bGVnYWxQZXJzb25BQkMja2V5In0.eyJAaWQiOiJkaWQ6d2ViOmV4YW1wbGUub3JnOmxlZ2FsUGVyc29uQUJDIiwiQHR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiXSwiaXNzdWVyIjoiZGlkOndlYjpleGFtcGxlLm9yZzpsZWdhbFBlcnNvbkFCQyIsInZhbGlkRnJvbSI6IjIwMjQtMDEtMDFUMDA6MDA6MDBaIiwidmFsaWRVbnRpbCI6IjIwMjQtMDQtMDFUMDA6MDA6MDBaIiwiY3JlZGVudGlhbFN1YmplY3QiOlt7IkBpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWxQZXJzb25BQkMiLCJAdHlwZSI6Imd4OmxlZ2FsUGVyc29uIiwiZ3g6bGVnYWxOYW1lIjoiTGVnYWwgUGVyc29uIEFCQyIsImd4OmxlZ2FsUmVnaXN0cmF0aW9uTnVtYmVyIjp7IkBpZCI6Imh0dHBzOi8vZ2FpYS14LmV1L2xlZ2FsUmVnaXN0cmF0aW9uTnVtYmVyX1ZDLmpzb24ifSwiZ3g6aGVhZHF1YXJ0ZXJBZGRyZXNzIjp7Imd4OmNvdW50cnlTdWJkaXZpc2lvbkNvZGUiOiJGUi1JREYifSwiZ3g6bGVnYWxBZGRyZXNzIjp7Imd4OmNvdW50cnlTdWJkaXZpc2lvbkNvZGUiOiJGUi1JREYifX1dfQ.RIQKYwKsYEhH9p3m9tbG6zQKae3A7Qz3oAHXMI9RwXYVCL-euaBG7fWGTQ_F6yqWSPeQ6veHqkxKkvtdLIkSSpxZRJCtQs2HiORQX3tc21dkqtziKJIDJhmIBIq-2zDToPb5D4Yb_ryP0aTgcnBavAuiNCf7x3_gS6tBtYd_ZNnh3cifFiLGLop6PUhqhaTEYBlw1ou-28XUCHPeaarGrmxyZzxiBV_3J5hAe8XvfnFo9Y__LcbuOjNMsU2kKhI9otw9Ll4C8IZ9Qsqdq52QFCvkbvtcvX_3IJpzyxSS7TxOXAPPwYbYV_u7tgygPRvvmQG99Q651y62tQGA_B6Eqg",
  "type": "EnvelopedVerifiableCredential"
}
```

</details>

### Verifiable Presentation

If the `@type` is "VerifiablePresentation", the property `verifiableCredential` MUST be defined.
The value of `verifiableCredential` property MUST be an array of one or
several [Enveloped Verifiable Credentials](#enveloped-verifiable-credential). A Verifiable Presentation MUST have :

- a [`@type`](#types),
- a [`verifiableCredential`](#verifiable-credential) array of `EnvelopedVerifiableCredential`,

<details>
<summary>Example Verifiable Presentation</summary>

Below is an example of a Verifiable Presentation containing the example from
the [Enveloped Verifiable Credential](#enveloped-verifiable-credential)
chapter.

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@id": "https://gaia-x.eu/verifiablePresentation/1",
  "type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": [
    {
      "@context": "https://www.w3.org/ns/credentials/v2",
      "id": "data:application/vc+ld+json+jwt;eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmc6bGVnYWxQZXJzb25BQkMja2V5In0.eyJAaWQiOiJkaWQ6d2ViOmV4YW1wbGUub3JnOmxlZ2FsUGVyc29uQUJDIiwiQHR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiXSwiaXNzdWVyIjoiZGlkOndlYjpleGFtcGxlLm9yZzpsZWdhbFBlcnNvbkFCQyIsInZhbGlkRnJvbSI6IjIwMjQtMDEtMDFUMDA6MDA6MDBaIiwidmFsaWRVbnRpbCI6IjIwMjQtMDQtMDFUMDA6MDA6MDBaIiwiY3JlZGVudGlhbFN1YmplY3QiOlt7IkBpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvbGVnYWxQZXJzb25BQkMiLCJAdHlwZSI6Imd4OmxlZ2FsUGVyc29uIiwiZ3g6bGVnYWxOYW1lIjoiTGVnYWwgUGVyc29uIEFCQyIsImd4OmxlZ2FsUmVnaXN0cmF0aW9uTnVtYmVyIjp7IkBpZCI6Imh0dHBzOi8vZ2FpYS14LmV1L2xlZ2FsUmVnaXN0cmF0aW9uTnVtYmVyX1ZDLmpzb24ifSwiZ3g6aGVhZHF1YXJ0ZXJBZGRyZXNzIjp7Imd4OmNvdW50cnlTdWJkaXZpc2lvbkNvZGUiOiJGUi1JREYifSwiZ3g6bGVnYWxBZGRyZXNzIjp7Imd4OmNvdW50cnlTdWJkaXZpc2lvbkNvZGUiOiJGUi1JREYifX1dfQ.RIQKYwKsYEhH9p3m9tbG6zQKae3A7Qz3oAHXMI9RwXYVCL-euaBG7fWGTQ_F6yqWSPeQ6veHqkxKkvtdLIkSSpxZRJCtQs2HiORQX3tc21dkqtziKJIDJhmIBIq-2zDToPb5D4Yb_ryP0aTgcnBavAuiNCf7x3_gS6tBtYd_ZNnh3cifFiLGLop6PUhqhaTEYBlw1ou-28XUCHPeaarGrmxyZzxiBV_3J5hAe8XvfnFo9Y__LcbuOjNMsU2kKhI9otw9Ll4C8IZ9Qsqdq52QFCvkbvtcvX_3IJpzyxSS7TxOXAPPwYbYV_u7tgygPRvvmQG99Q651y62tQGA_B6Eqg",
      "type": "EnvelopedVerifiableCredential"
    }
  ]
}
```

</details>

### Enveloped Verifiable Presentation

Just like an [Enveloped Verifiable Credential](#enveloped-verifiable-credential), an Enveloped Verifiable Presentation
is a representation of a [Verifiable Presentation](#verifiable-presentation) in the form of a basic JSON object
containing
an `application/vp+ld+jwt` [data URL](https://www.rfc-editor.org/rfc/rfc2397).

This data URL expresses a JWS secured Verifiable Presentation.
The same headers as [Verifiable Credentials](#verifiable-credential) are used in a Verifiable Presentation VC-JWT
except:

- the `typ` header is set to `vp+ld+jwt`
- the `cty` header is set to `vp+ld+json`

<details>
<summary>Example Enveloped Verifiable Presentation</summary>

Below is a representation of the [Verifiable Presentation example](#verifiable-presentation) as an Enveloped Verifiable
Presentation.

```json
{
  "@context": "https://www.w3.org/ns/credentials/v2",
  "id": "data:application/vp+ld+jwt;eyJhbGciOiJQUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImtpZCI6ImRpZDp3ZWI6ZXhhbXBsZS5vcmc6bGVnYWxQZXJzb25BQkMja2V5In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiXSwiQGlkIjoiaHR0cHM6Ly9nYWlhLXguZXUvdmVyaWZpYWJsZVByZXNlbnRhdGlvbi8xIiwidHlwZSI6WyJWZXJpZmlhYmxlUHJlc2VudGF0aW9uIl0sInZlcmlmaWFibGVDcmVkZW50aWFsIjpbeyJAY29udGV4dCI6Imh0dHBzOi8vd3d3LnczLm9yZy9ucy9jcmVkZW50aWFscy92MiIsImlkIjoiZGF0YTphcHBsaWNhdGlvbi92YytsZCtqc29uK2p3dDtleUpoYkdjaU9pSlFVekkxTmlJc0luUjVjQ0k2SW5aaksyeGtLMnB6YjI0cmFuZDBJaXdpWTNSNUlqb2lkbU1yYkdRcmFuTnZiaUlzSW10cFpDSTZJbVJwWkRwM1pXSTZaWGhoYlhCc1pTNXZjbWM2YkdWbllXeFFaWEp6YjI1QlFrTWphMlY1SW4wLmV5SkFhV1FpT2lKa2FXUTZkMlZpT21WNFlXMXdiR1V1YjNKbk9teGxaMkZzVUdWeWMyOXVRVUpESWl3aVFIUjVjR1VpT2xzaVZtVnlhV1pwWVdKc1pVTnlaV1JsYm5ScFlXd2lYU3dpYVhOemRXVnlJam9pWkdsa09uZGxZanBsZUdGdGNHeGxMbTl5Wnpwc1pXZGhiRkJsY25OdmJrRkNReUlzSW5aaGJHbGtSbkp2YlNJNklqSXdNalF0TURFdE1ERlVNREE2TURBNk1EQmFJaXdpZG1Gc2FXUlZiblJwYkNJNklqSXdNalF0TURRdE1ERlVNREE2TURBNk1EQmFJaXdpWTNKbFpHVnVkR2xoYkZOMVltcGxZM1FpT2x0N0lrQnBaQ0k2SW1oMGRIQnpPaTh2WlhoaGJYQnNaUzV2Y21jdmJHVm5ZV3hRWlhKemIyNUJRa01pTENKQWRIbHdaU0k2SW1kNE9teGxaMkZzVUdWeWMyOXVJaXdpWjNnNmJHVm5ZV3hPWVcxbElqb2lUR1ZuWVd3Z1VHVnljMjl1SUVGQ1F5SXNJbWQ0T214bFoyRnNVbVZuYVhOMGNtRjBhVzl1VG5WdFltVnlJanA3SWtCcFpDSTZJbWgwZEhCek9pOHZaMkZwWVMxNExtVjFMMnhsWjJGc1VtVm5hWE4wY21GMGFXOXVUblZ0WW1WeVgxWkRMbXB6YjI0aWZTd2laM2c2YUdWaFpIRjFZWEowWlhKQlpHUnlaWE56SWpwN0ltZDRPbU52ZFc1MGNubFRkV0prYVhacGMybHZia052WkdVaU9pSkdVaTFKUkVZaWZTd2laM2c2YkdWbllXeEJaR1J5WlhOeklqcDdJbWQ0T21OdmRXNTBjbmxUZFdKa2FYWnBjMmx2YmtOdlpHVWlPaUpHVWkxSlJFWWlmWDFkZlEuUklRS1l3S3NZRWhIOXAzbTl0Ykc2elFLYWUzQTdRejNvQUhYTUk5UndYWVZDTC1ldWFCRzdmV0dUUV9GNnlxV1NQZVE2dmVIcWt4S2t2dGRMSWtTU3B4WlJKQ3RRczJIaU9SUVgzdGMyMWRrcXR6aUtKSURKaG1JQklxLTJ6RFRvUGI1RDRZYl9yeVAwYVRnY25CYXZBdWlOQ2Y3eDNfZ1M2dEJ0WWRfWk5uaDNjaWZGaUxHTG9wNlBVaHFoYVRFWUJsdzFvdS0yOFhVQ0hQZWFhckdybXh5Wnp4aUJWXzNKNWhBZThYdmZuRm85WV9fTGNidU9qTk1zVTJrS2hJOW90dzlMbDRDOElaOVFzcWRxNTJRRkN2a2J2dGN2WF8zSUpwenl4U1M3VHhPWEFQUHdZYllWX3U3dGd5Z1BSdnZtUUc5OVE2NTF5NjJ0UUdBX0I2RXFnIiwidHlwZSI6IkVudmVsb3BlZFZlcmlmaWFibGVDcmVkZW50aWFsIn1dfQ.jnEqD2HH7eNnzRPwTjwsFigyENPozdDzmksXjevGNiH4hWJGLoM-765IP1mEE-tsLi2tMXQ6TeWIfw_6NkpY0vo_FUXWBBlj0IgMbxbt0gQwHRW9Ph3SVKQMCdIfp_pmdWPCEUrr_HxjkdiZpF1fa4qGSYBYl6tRSf1N0iCY0SzKvStI-EiudwHtlSygcqjxNq1jdpZtQyjYa_golZmyBdX7BYUUkcY30vypKTjMgBLHlZOzIljdiLKcm_MfDGEBt-Ha_qxpKpwRZoMFhsq89RXeExpeAw8Vg3ZR7yWsmP3T-7DDrZ5sadpNyCDXpryvm2UoDs__M4lEvkl3HIy9LQ",
  "type": "EnvelopedVerifiablePresentation"
}
```

</details>

## Gaia-X compliance input/output

The Gaia-X Compliance service

```mermaid
flowchart LR

in[Verifiable Presentation]
compliance[Gaia-X Compliance]
out[Verifiable Credential]

in --> |input|compliance --> |output|out
```

### Input

The input of the Gaia-X Compliance service is a VC-JWT `Verifiable Presentation` that might contain:

- one or more [Enveloped Verifiable Credentials](#enveloped-verifiable-credential)
- one or more [Verifiable Credentials](#verifiable-credential)

Such a Verifiable Credential MAY or MAY NOT be covered by
the [Gaia-X Compliance rules](http://docs.gaia-x.eu/policy-rules-committee/trust-framework/).

The following example contains fake, placeholder attributes for `Participant` and `ServiceOffering`, which are not valid
against the Gaia-X Credential Schema.

<details>
<summary>Example of Compliance input</summary>

> :warning: Due to the lack of readability of JWTs, the following credentials are not signed, for more complete functional cases please take a look at these [examples](https://gitlab.com/gaia-x/technical-committee/identity-credentials-and-access-management-working-group/icam/-/tree/main/docs/examples/)

```json title="compliance_input.json"
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "type": "VerifiablePresentation",
  "issuer": "did:web:gaia-x.eu",
  "validUntil": "2024-07-11T07:51:07.168+00:00",
  "verifiableCredential": [
    {
      "id": "https://gaia-x.eu/participant.json",
      "type": "VerifiableCredential",
      "issuer": "did:web:gaia-x.eu",
      "validFrom": "2024-01-01T19:23:24Z",
      "validUntil": "2024-07-11T07:42:21.972+00:00",
      "credentialSubject": {
        "id": "https://gaia-x.eu/participant.json#cs",
        "type": "gx:LegalPerson",
        "https://schema.org/name": "GAIA-X",
        "gx:registrationNumber": {
          "id": "https://gaia-x.eu/gaia-x-lrn.json#cs"
        },
        "gx:headquartersAddress": {
          "type": "gx:Address",
          "gx:countryCode": "BE"
        },
        "gx:legalAddress": {
          "type": "gx:Address",
          "gx:countryCode": "BE"
        }
      }
    },
    {
      "id": "https://gaia-x.eu/gaia-x-lrn.json",
      "issuer": "did:web:gaia-x.eu",
      "type": "VerifiableCredential",
      "validFrom": "2024-05-15T12:10:23.900Z",
      "validUntil": "2024-07-11T07:43:05.752+00:00",
      "credentialSubject": {
        "type": "gx:VatID",
        "id": "https://gaia-x.eu/gaia-x-lrn.json#cs",
        "gx:vatID": "BE0762747721",
        "gx:countryCode": "BE"
      }
      
    },
    {
      "issuer": "did:web:gaia-x.eu",
      "id": "https://gaia-x.eu/gaia-x-tsandcs.json",
      "type": "VerifiableCredential",
      "validFrom": "2024-05-27T09:12:35.754Z",
      "validUntil": "2024-07-11T07:43:30.274+00:00",
      "credentialSubject": {
        "gx:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n\nThe keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD).",
        "type": "gx:GaiaXTermsAndConditions",
        "id": "https://gaia-x.eu/gaia-x-tsandcs.json#cs"
      }
      
    },
    {
      "id": "https://gaia-x.eu/service.json",
      "issuer": "did:web:gaia-x.eu",
      "type": "VerifiableCredential",
      "validFrom": "2024-07-23T13:36:54.648Z",
      "validUntil": "2024-07-25T15:17:11.243+00:00",
      "credentialSubject": {
        "type": "gx:ServiceOffering",
        "gx:providedBy": {
          "@id": "https://gaia-x.eu/participant.json#cs"
        },
        "gx:policy": "",
        "gx:termsAndConditions": {
          "gx:URL": "http://termsandconds.com",
          "gx:hash": "d8402a23de560f5ab34b22d1a142feb9e13b3143"
        },
        "gx:dataAccountExport": {
          "gx:requestType": "API",
          "gx:accessType": "digital",
          "gx:formatType": "application/json"
        },
        "@id": "https://gaia-x.eu/service.json#cs"
      }
    }
  ]
}
```

</details>

### Output

The output of the Gaia-X Compliance service is a VC-JWT `VerifiableCredential` containing the `id` and `hash` of the
compliant `VerifiableCredential` from the input.

<details>
<summary>Example of Compliance output</summary>

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "type": [
    "VerifiableCredential",
    "gx:ComplianceCredential"
  ],
  "id": "https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203",
  "issuer": "did:web:compliance.lab.gaia-x.eu:development",
  "validFrom": "2024-07-24T15:18:28.376Z",
  "validUntil": "2024-10-22T15:18:28.355Z",
  "credentialSubject": {
    "id": "https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203#cs",
    "gx:evidence": [
      {
        "id": "https://gaia-x.eu/participant.json",
        "type": "gx:ComplianceEvidence",
        "gx:integrity": "sha256-578b2fa4ec5d83317f7356dfb11f656c14ac3b1705ee276d09ed76871bf53b29",
        "gx:integrityNormalization": "RFC8785:JCS",
        "gx:engineVersion": "2.2.0",
        "gx:rulesVersion": "PRLD-24.04_pre",
        "gx:originalType": "gx:LegalPerson,VerifiableCredential"
      },
      {
        "id": "https://gaia-x.eu/gaia-x-tsandcs.json",
        "type": "gx:ComplianceEvidence",
        "gx:integrity": "sha256-3962bfc58471f19e8e5d5ea05652725ca0bb8b62af27cfdd8d9022c69b585387",
        "gx:integrityNormalization": "RFC8785:JCS",
        "gx:engineVersion": "2.2.0",
        "gx:rulesVersion": "PRLD-24.04_pre",
        "gx:originalType": "gx:GaiaXTermsAndConditions,VerifiableCredential"
      },
      {
        "id": "https://gaia-x.eu/gaia-x-lrn.json",
        "type": "gx:ComplianceEvidence",
        "gx:integrity": "sha256-3e374271b13e1241eda27c672de70c3d4d497a4b4c8f02f287fe3aa61d789fd9",
        "gx:integrityNormalization": "RFC8785:JCS",
        "gx:engineVersion": "2.2.0",
        "gx:rulesVersion": "PRLD-24.04_pre",
        "gx:originalType": "gx:VatID,VerifiableCredential"
      },
      {
        "id": "https://gaia-x.eu/service.json",
        "type": "gx:ComplianceEvidence",
        "gx:integrity": "sha256-1b1a7bb3545891ca912ee29425b74d842eec09c1c4f6847571708d0888601457",
        "gx:integrityNormalization": "RFC8785:JCS",
        "gx:engineVersion": "2.2.0",
        "gx:rulesVersion": "PRLD-24.04_pre",
        "gx:originalType": "gx:ServiceOffering,VerifiableCredential"
      }
    ]
  }
}
```
</details>
