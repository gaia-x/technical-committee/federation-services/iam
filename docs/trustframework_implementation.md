## Trust Framework Implementation 

This section is dedicated to illustrate how to use, extend, and customize the semantic model of the trust framework.
This is achievable in 2 steps:

1. define and declare the trust anchor through the use of **TrustAnchorCredential specialisation** used to define trust issuers, the semantic model defined in the scope of the trust anchor(vocabularies), a preferred trusted list tecnology/mode.
2. define the delegation chain through the use of **PartyCredential specialisation** 


## Trust Anchor Credential specialisation examples

Several specialisations of the **TrustAnchorCredential** can be defined, for example:

- **Organization Trust Anchor** – a self-issued credential that entitles an organization to define:
   - **scope** - *Organization Credential Management* (OCM).
   - **trusted issuer** - the organisation itself.
   - **vocabularies** - defines the semantics of *Roles/Identity Attributes* and **Domain Specific Credentials** that are valid in the defined scope.
   - **trusted list** - to assign and revoke *Roles/Identity Attributes* and **Domain Specific Credentials** to its parties (users, natural persons, endpoint services, etc) by issuing **PartyCredentials**.

- **Ecosystem Trust Anchor** - a self-issued credential that entitles an Ecosystem operator to define:
   - **scope** - manage an *Ecosystem* (onboarding/offboarding/role assignment etc.).
   - **trusted issuer** - the Ecosystem itself. <!-- the Ecosystem Governance Authority? -->
   - **vocabularies** - defines the semantic of *Roles/Identity Attributes* and **Domain Specific Credentials** valid in the defined scope.
   - **trusted list** - to assign and revoke *Roles/Identity Attributes* and **Domain Specific Credentials** to its members (other Participants) issuing **MembershipPartyCredentials**.

- **Gaia-X Compliance Trust Anchor** - a Gaia-X-issued credential that entitles an organisation to issue attestations about specific claims in the context of Gaia-X Compliance.
   - **scope** - issue attestations used to attest Gaia-X Compliance.
   - **trusted issuer** - the organisation accredited by Gaia-X to issue attestations in the defined scope.
   - **vocabularies** - defines the semantic applicable to issue attestations in the defined scope.
   - **trusted list** - to assign and revoke attestations issued to Gaia-X Providers.


## Party Credential Specialisation examples


Here, some of the possible Party Credential specialisations are defined.

### Natural Person Party Credential

This credential is issued by a Participant to entitle a natural person (usually one of his users/employees) to interact with other Relying Parties belonging to other Participants.

This is how `Natural Person Party Credential` will be defined by the following attributes in addition to Party Credential attributes:

<!-- I guess that the table below should be reviewed -->

| Attribute      | Type.Value/Voc | Mandatory | Comment                       |
|----------------|----------------|-----------|-------------------------------|
| `gx:givenName` | String         | Yes       | Name of the natural person    |
| `gx:surname`   | String         | Yes       | Surname of the natural person |

### Legal Person Party Credential

This credential is issued by a Legal Person Participant to entitle another legal person (usually one of his users <!-- could we perhaps use a better word than users? -->  ) to interact with other Relying Parties belonging to other Participants on its behalf.

This is how `Legal Person Party Credential` will be defined by the following attributes in addition to Party Credential attributes:

| Attribute                   | Type.Value/Voc | Mandatory | Comment                                         |
|-----------------------------|----------------|-----------|-------------------------------------------------|
| `gx:organizationIdentifier` | String         | Yes       | Organization Identifier used in the eIDAS rules |

### Service Party Credential

This credential is issued by a Participant to entitle an automated service (usually an automated process) to interact with other Relying Parties belonging to other Participants.

This is how `Service Party Credential` will be defined by the following attributes in addition to Party Credential attributes

| Attribute    | Type.Value/Voc | Mandatory | Comment                                               |
|--------------|----------------|-----------|-------------------------------------------------------|
| `gx:baseURL` | URI            | Yes       | The base URL endpoint where the service is accessible |

### Membership Party Credential

This credential is issued by a LegalParticipant that runs an <!-- we could replace ecosystem by data space --> ecosystem to another Participant in order to attest his Membership status.

<!-- to be added attributes specific to the Membership Credential/check with the Lab about our Membership Credential -->




## Access rights delegation example - Employee Authentication

### Problem Statement
Consider the scenario where a company or a department wants to provide its employees with secure access to various cloud services (e.g., email, document storage, collaboration tools) without relying on a central identity provider. 
The ecosystem uses the decentralized identity and access management based on Self-Sovereign Identity (SSI)  to enhance security, privacy, and flexibility. The goal is to design a system where employees can authenticate themselves to an ecosystem where the company/department is member of and access cloud services based on their roles and entitlements, without the need for a central identity provider. For security considerations, with only one single step all issued access rights must be revoked when an employee leaves the company or changes roles. The management of the access rights should be done by the respective manager and the HR department and not from a central administrator.

### Types of Credentials and Issuers involved

#### Employee Credential (**specialised PartyCredential**):
The company HR department is managing the employees and therefore issues an `EmployeeCredential` to each employee. 

<!-- This credential type and some attributes like Company ID must be standardized globally to enable the verifier to implement a generic joined verifiable presentation for authentication without the need to know and manage the employees on each ecosystem.-->

 This employee credential contains the employee's unique identifier, role, department and unique company ID (unique identifier used in the onboarding phase of the company to the ecosystem). The HR department is the issuer of this credential, and it serves as the foundation for the employee's identity within the organization.  The `EmployeeCredential` is used to verify the employee's identity and affiliation with the company. 
 The Company ID should be registered in the membership trustlist of the ecosystem and in parallel used in the separate issued membership credential. The HR must revoke the employee credential in case he/she leaves the company and the employee should no longer be able to access any cloud services on behalf of the company.

- **Type**: `EmployeeCredential (Specialised Party-Credential)`
- **Issuer**: HR Department
- **Subject**: Employee
- **Company ID**: Global unique identifier for the company or organization (eg. GLEIF ID, VAT,), same across all employees. The Company ID should be used in the membership trustlist and membership credential used to verify the authenticity of the credential. 
- **Emp Attributes**: Like, Employee ID, Name, Role, Department 

The `EmployeeCredential` is a prerequisite for receiving `AccessEntitlementCredentials` and is used to establish the employee's identity within the organization. The Employee Credential type and the attributes must be standardized 

### Access Entitlement Credentials:
- **Type**: `AccessEntitlementCredential (for each access right individual credentials must be issued)`
- **Issuer**: Employee Manager
- **Attributes**: Employee ID, Access Rights (specific cloud services the employee can access)
- **Purpose**: Specifies the services the employee is entitled to access.
- **Scope**: Issued by the respective manager to grant access to specific services based on the employee's role and responsibilities.
- **Revocation**: Manager who can revoke the credential to dynamically manage access rights. <!--could they differ from the Issuer/Employee manager?>
- **Employee ID**: The employee's `EmployeeCredential` is a prerequisite for receiving `AccessEntitlementCredentials`.

The `AccessEntitlementCredential` is issued by the respective manager to grant access to specific cloud services based on the employee's role and responsibilities. The manager can revoke these credentials to dynamically manage access rights. The `EmployeeCredential` is a prerequisite for receiving `AccessEntitlementCredentials`. the Manager must be entitled to issue such credential, therefore the manager must have a valid `ManagerCredential` issued by the HR department. In addition, the manager must be a managing member of the ecosystem and the company ID must be part of the credential to ensure the manager is a valid member of the company. Therefore, the manager is registered in the membership trustlist of the specific ecosystem services and in parallel he holds a valid issued membership credential.

<!--is the Membership credential also for natural persons?-->

### Authentication Process

1. **Employee Presents Credentials**: When accessing a service, the employee presents its `EmployeeCredential` and `AccessEntitlementCredential`.
2. **Verification**: The service verifies:
   - The authenticity of the credentials (ensuring they are issued by the HR department and the respective manager).
   - The validity of the credentials (not revoked).
   - The attributes match the access control policies (e.g., the employee's role and entitlements align with the service being accessed).

### Revocation

- **Revocation List or Registry**: The HR department and managers can revoke credentials by adding them to a revocation list or registry that the services check during the authentication process.
- **Issuer**: HR Department for `EmployeeCredential`, Manager for `AccessEntitlementCredential`.
- **Mechanism**: Could use a decentralized identifier (DID) revocation mechanism or a centralized revocation service, depending on the architecture.

### Implementation Considerations

- **Decentralized Identifiers (DIDs)**: Use DIDs for employees, HR, and managers to ensure a decentralized and self-sovereign identity system.
- **Verifiable Data Registry**: Implement a verifiable data registry to store DIDs for validation.
- **Privacy**: Ensure that the system only reveals the minimum necessary information for authentication and access control to protect employee privacy.
- **Interoperability**: Design the credential schema to be interoperable with W3C Verifiable Credentials standards and compatible with the cloud services' access control mechanisms.
- **Trust** An ecosystem may have many trustlists (Gaia-X , XFCS Ecosystem Member, Service Manager). 
