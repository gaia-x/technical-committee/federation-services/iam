# Changelog

## 2024 July release (24.07)

- Updated chapter "Credential Format"
- New chapter "Trust Anchor Credential and Party Credential"
- New chapter "OpenID Connect for Verifiable Credentials"
- New chapter "Signature Credential"
- New chapter "Trust Framework implementation", containing Trust Anchor Credential specialisation examples and Party Credential specialisation examples and an access rights delegation example.

