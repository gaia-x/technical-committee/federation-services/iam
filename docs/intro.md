# Introduction and Scope of the Document

<!-- to be integrated-->

This document covers the Gaia-X Identity, Credential and Access Management(ICAM) specifications.

The scope of the document is to describe the components for “Authorization & Authentication” which shall deliver core
functionalities for authorization, access management and authentication as well as services around it to Gaia-X Participants, with the purpose to join the trustful environment of the Gaia-X Ecosystem. 

Note: this document does not describe how to replace an already established IAM System within a Gaia-X participant environment or how to operate it within the Gaia-X participant environment.
