# OpenID Connect for Verifiable Credentials

OpenID Connect for Verifiable Credentials (OIDC4VC) is a non-official name for collection of OpenID Connect specifications
allowing Verifiable Credential and Verifiable Presentation exchanges.

The two main specifications that are relevant in the Gaia-X Ecosystem are:
- [OpenID Connect for Verifiable Credential Issuance (OIDC4VCI)](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html)
- [OpenID Connect for Verifiable Presentations (OIDC4VP)](https://openid.github.io/OpenID4VP/openid-4-verifiable-presentations-wg-draft.html)

## OpenID Connect for Verifiable Credential Issuance

This specification is based on the [OAuth 2.0 specification](https://www.rfc-editor.org/info/rfc6749) and allows an issuer
to communicate with a holder and its wallet in order to issue Verifiable Credentials in a secure manner.

It's a great protocol for machine-to-end-user credential issuance but also from machine-to-machine issuance by using 
OAuth 2.0's battle tested and widely adopted standards.

Verifiable Credentials will be exchanged by using the VC-JWT format.

> ⚠️ The current OIDC4VCI specification is still a draft meaning that the implementation might evolve with time. The 
> Gaia-X Lab is headed towards the first approved specification, meanwhile the latest draft will be implemented.

## OpenID Connect for Verifiable Presentations

Just like OIDC4VCI, OIDC4VP is based on the [OAuth 2.0 specification](https://www.rfc-editor.org/info/rfc6749) in order
to allow a holder and its wallet to present one or multiple Verifiable Credentials to a verifier through a Verifiable 
Presentation.

This can also be a machine-to-end-user protocol in addition to a machine-to-machine protocol thanks to OAuth 2.0 
standards.

Verifiable Presentations will be exchanged by using the VC-JWT format.

> ⚠️ The current OIDC4VP specification is still a draft meaning that the implementation might evolve with time. The
> Gaia-X Lab is headed towards the first approved specification, meanwhile the latest draft will be implemented.

## Usage

Both these protocols will be used each time a Verifiable Credential needs to be produced or consumed by the Gaia-X 
Clearing House hence securing the exchange with an authentication and proof of possession mechanism.

## Cloud/Enterprise Wallet

As most of the exchanges will be in a machine-to-machine environment, a cloud or enterprise wallet will be used although
a specific solution hasn't been chosen to this date.
