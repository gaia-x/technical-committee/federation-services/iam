# Signature Credential

## Multiple Signatures using SignatureCredential specializations
The **SignatureCredential** in the trust model defined by the ICAM Document, represents a general purpose and machine readable *signature* that Participants are asked to provide in several contexts, one of the most important of these contexts is represented by the [Data Transaction](https://docs.gaia-x.eu/technical-committee/data-exchange/23.11/dewg/#data-transaction) section of the Data Exchange Document(version 23.11) where this credential will be used to sign **Data Usage Agreement** and **Data Product Usage Contract**.

The concept behind this credential is the same as the credentials issued by the compliance service, to issue Participant Credentials and  Service Offering Credentials (using the "credentialSubject" claim), where the **credentialSubject** consists in:

| Attribute   | Type.Value/Voc | Mandatory | Comment                                                                                                                                                                                                                                     |
|-------------|----------------|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`      | string         | Yes       | Indicating the *type* of the subject to be Signed                                                                                                                                                                                           |
| `id`        | URI            | Yes       | A resolvable single URI as defined in the [VC Specification](https://www.w3.org/TR/vc-data-model-2.0/#identifiers) identifying the subject to be Signed                                                                                     |
| `digestSRI` | String         | Yes       | Integrity of related Resources as defined in [W3C VC Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#integrity-of-related-resources) to ensure that referenced resource is not changed/modified/tempered(still identical in time) | 

Another important factor is represented by the fact that, issuing and signing this credential with a **verificationMethod** bound to a certificate that has legal relevance (e.g eIDAS) gives it the same level of trust, enabling the possibility to check the `gx:legalValidity` property of the **Signature Check Type** (see `gx:signers` property).

### SignedAgreementCredential

The **SignedAgreementCredential** is a specialization of **SignatureCredential** that represents a *Revocable Signed Agreement* given by a participant to a specific subject (e.g. Data Usage Agreement, GDPR Agreement, etc.) and the only difference stands in the fact that it can be revoked.

 
### Data Usage Agreement Example

In a Data Transaction the **Data Provider**(did:web:provider.com) and a **Data Consumer**(did:web:consumer.com) must agree to a **Data Usage Agreement** (**id** "https://provider.com/data-usage-contract.654321") and this is done treating "Signatures" as a **Verifiable Credential** that each Participant can issue and sign. 
In this way, when all Participants have issued their SignatureCredential referencing the contract it will be possible to create Verifiable Presentations that contain all the Signatures.


Here is an example of how CredentialSignature can be used:



```json

{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "type": [
    "VerifiableCredential"
  ],
  "id": "https://consumer.com/data-usage-contract-signatures.123456",
  "issuer": "did:web:consumer.com",
  "validFrom": "2023-07-28T12:31:49.074Z",
  "validUntil": "2023-10-26T12:31:49.074Z",
  "credentialSubject": {
    "type": "gx:dataUsageAgreement",
    "id": "https://provider.com/data-usage-contract.654321",
    "digestSRI": "sha384-lHKDHh0msc6pRx8PhDOMkNtSI8bOfsp4giNbUrw71nXXLf13nTqNJoRp3Nx+ArVK"
  },
  "credentialStatus": {
    "id": "https://consumer.com/status/1#127",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "revocation",
    "statusListIndex": "127",
    "statusListCredential": "https://consumer.com/credentials/status/1"
  }
}
```

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "type": [
    "VerifiableCredential"
  ],
  "id": "https://provider.com/data-usage-contract-signatures.987",
  "issuer": "did:web:provider.com",
  "validFrom": "2023-07-25T10:31:49.074Z",
  "validUntil": "2023-10-25T10:31:49.074Z",
  "credentialSubject": {
    "type": "gx:dataUsageAgreement",
    "id": "https://provider.com/data-usage-contract.654321",
    "digestSRI": "sha384-lHKDHh0msc6pRx8PhDOMkNtSI8bOfsp4giNbUrw71nXXLf13nTqNJoRp3Nx+ArVK"
  },
  "credentialStatus": {
    "id": "https://provider.com/status/3#125221",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "revocation",
    "statusListIndex": "125221",
    "statusListCredential": "https://provider.com/credentials/status/3"
  }
}

```

## Multiple Signatures using Proof Set and Proof Chain
In cases where a credential must be signed by multiple parties in a non-revocable way, whether the signing order matters or not, the proof chain and proof set mechanism specified in the [VC Data Integrity](https://www.w3.org/TR/vc-data-integrity/#add-proof-set-chain) standard must be used. This mechanism is particularly essential for contract negotiation and compliance services credentials involving many entities.

To ensure cryptographic integrity and crypto agility, the signature must not be embedded inside the credentialSubject. The definition provided by the [Gaia-X Ontology](https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blob/develop/single-point-of-truth/cryptography.yaml?ref_type=heads) is reserved for special use cases and should not be used in such scenarios.

For compatibility reasons the multiple-signed VC should be W3C credential and wrapped by a VC-JWT credential signed by the issuer
