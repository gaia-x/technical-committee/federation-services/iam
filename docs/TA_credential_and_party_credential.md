# TrustAnchor Credential

The Trust Anchor Credential is based on the [Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) and has the purpose to provide a machine-readable representation of the accreditation of a Trust Anchor for a specific scope. This credential enables the usage of Party Credentials, described later in this chapter, by defining their accredited issuers. Furthermore, the Trust Anchor credential supports the cooperation and interoperability between organization/ecosystems/data spaces, by easing the use of external Trust Anchors.

The **TrustAnchorCredential** mainly defines:

<!-- I modified the reference below from Party Credential to Credential since I understood that the TA Credential is usable not just for Trust Anchors of Party Credentials -->

- The **Scope** within which the Trust Anchor is accredited (within which the issued credentials are considered valid).  
- The **TrustedIssuer** <!-- Is there a reason why not to name it TrustAnchor? --> entitled to issue **Credentials** in the above **Scope**
- The **Vocabularies** (expressed in SHACL) that semantically define the **Credentials** which can be issued by the **TrustedIssuer** in the defined **Scope**.
- The **Trusted List** used to check DIDs issued by the Trust Anchor.

The `Trust Anchor Credential` is defined by the following attributes:

<!-- are there any other attributes to be considered?where do we see how long the TA Credential is valid?Maybe the revocation mechanism has to be defined better --> 

| Attribute                | Type.Value/Voc    | Mandatory | Comment                                                                                                                                                                                                                                  |
|--------------------------|-------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `gx:scopeDescription`    | String            | No        | A description of the scope of the Trust Anchor                                                                                                                                                                                           |
| `gx:trustIssuer`         | DID               | Yes       | A resolvable link to the holder verificationMethod to be used to uniquely identify ONE and ONLY key pair                                                                                                                                 |
| `gx:vocabularies`        | URI[]             | No        | A list of URIs pointing to the vocabularies (SHACL) semantically describing the Trust Anchor's scope <!-- could we be clearer here?reference to the semantic applicable to issue attestations in the defined scope would be clearer? --> |
| `gx:trustedListKind`     | KindOfTrustedList | No        | Which kind of implementation of the trusted list is used to check DID issued by Trust Anchors                                                                                                                                            | 
| `gx:trustedListEndpoint` | URI               | No        | The address of the above trusted list                                                                                                                                                                                                    | 

The **KindOfTrustedList** type defines the list of the identified implementations of Trusted Lists:

- Gaia-X Trusted List Generic REST API Specification
- Gaia-X IPFS (with ETSI TS 119 612 format)
- TRAIN
- EBSI
- (other possible implementations)


**Important Note**
The *Gaia-X Trusted List Generic REST API Specification* is meant to be a simple API contract that for a given DID can return true if it is still valid. This will be very useful to enable integration of any custom trusted list not included in the defined list.


# Party Credential

The Party Credential is based upon the [Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/) and is the basement for all IAA Parties such as **Natural Persons** , **Services**, **Legal Persons**, etc. 
The general purpose party credential is intended to be extended into specialized Credentials (see **Party Credential Specializations**) 

The `Party Credential` is defined by the following attributes:

| Attribute                  | Type.Value/Voc   | Mandatory                                                | Comment                                                                                                                                        |
|----------------------------|------------------|----------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `gx:holder`                | URI              | Yes                                                      | A resolvable link to the holder verificationMethod to be used to uniquely identify ONE and ONLY key pair                                       |
| `odrl:hasPolicy`           | policy[] in ODRL | No                                                       | A list of `policy` expressed using ODRL                                                                                                        |
| `gx:identityAttributes`    | String[]         | No                                                       | A list of literals representing Identity Attributes to be used in a ABAC context                                                               | 
| `gx:identityRoles`         | String[]         | No                                                       | A list of literals representing Identity Roles to be used in a RBAC context                                                                    | 
| `gx:parentPartyCredential` | URI              | Yes if is delegated by another existing Party Credential | A resolvable link to the parent Party Credential where the gx:holder MUST be equal to the signer(issuer verificationMethod) of this credential | 

**VERY IMPORTANT NOTE**

The **credentialSubject.id** must identify the **DID Document** that contains the **verificationMethod** (keypair)  referenced by `gx:holder` property that *belong to/is controlled by* the **Credential Holder**.
With this solution, the PartyCredential VC can be either publicly published in case it contains no sensitive data or kept private in case it contains PII - Personal Identifiable Information.

## Private Party Credential

The **Party Credentials** containing PII are not considered to be published and reachable via their **id** to everybody, instead, they are intended to be stored in secure storage such as a wallet, secure storage device, secure vault storage, etc.
An example of this type of credential is the **NaturalPersonCredential**, issued by a Legal Participant to one of his users/employees, with the purpose to entitle a Natural Person to interact with Relying Parties(RP) in a certain context
This credential contains Name, Surname, identityAttributes, Roles, etc. and MUST NOT be published as Public Party Credentials (see later in this section) are. "Selective disclosure" during interaction with RP can be considered.

### Private Party Credential Example

The following **NaturalPersonPartyCredential** example represents the scenario where the Participant **did:web:did.actor:alice** is issuing a credential that <!-- self? --> asserts several claims (givenName, surname, idRoles etc).
This Credential is not published (the **id** is not public) and must be stored in the wallet of the holder. 

Note that the `gx:holder` property is a did:key referencing a keypair owned by the **Holder** that identifies the public key of the target wallet.

 <!-- in this example we can see more fields than those explained above - are they necessary?something more should be explained? -->


```json linenums="1" title="party_credential.json" hl_lines="4 11 17"

{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "id": "did:web:did.actor:alice:credentials:private#1222331234",
  "type": ["VerifiableCredential"],
  "issuer": "did:web:did.actor:alice",
  "validFrom": "2023-08-28T23:00:00Z",
  "credentialSubject": {
    "id": "did:key:z4MXj1wBzi9jUstyPMS4jQqB6KdJaiatPkAtVtGc6bQEQEEsKTic4G7Rou3iBf9vPmT5dbkm9qsZsuVNjq8HCuW1w24nhBFGkRE4cd2Uf2tfrB3N7h4mnyPp1BF3ZttHTYv3DLUPi1zMdkULiow3M1GfXkoC6DoxDUm1jmN6GBj22SjVsr6dxezRVQc7aj9TxE7JLbMH1wh5X3kA58H3DFW8rnYMakFGbca5CB2Jf6CnGQZmL7o5uJAdTwXfy2iiiyPxXEGerMhHwhjTA1mKYobyk2CpeEcmvynADfNZ5MBvcCS7m3XkFCMNUYBS9NQ3fze6vMSUPsNa6GVYmKx2x6JrdEjCk3qRMMmyjnjCMfR4pXbRMZa3i",
    "type": "gx:NaturalPersonPartyCredential",
    "odrl:hasPolicy": [],
    "gx:holder": "did:key:z4MXj1wBzi9jUstyPMS4jQqB6KdJaiatPkAtVtGc6bQEQEEsKTic4G7Rou3iBf9vPmT5dbkm9qsZsuVNjq8HCuW1w24nhBFGkRE4cd2Uf2tfrB3N7h4mnyPp1BF3ZttHTYv3DLUPi1zMdkULiow3M1GfXkoC6DoxDUm1jmN6GBj22SjVsr6dxezRVQc7aj9TxE7JLbMH1wh5X3kA58H3DFW8rnYMakFGbca5CB2Jf6CnGQZmL7o5uJAdTwXfy2iiiyPxXEGerMhHwhjTA1mKYobyk2CpeEcmvynADfNZ5MBvcCS7m3XkFCMNUYBS9NQ3fze6vMSUPsNa6GVYmKx2x6JrdEjCk3qRMMmyjnjCMfR4pXbRMZa3i",
    "gx:identityAttributes": ["IdAttributeOne","IdAttributeTwo"],
    "gx:identityRoles": ["IdRoleOne","IdRoleTwo"],
    "gx:givenName": "John",
    "gx:surname": "Doe"
  },
  "credentialStatus": [{
    "id": "https://did.actor/alice/credentials/status/3#94567",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "revocation",
    "statusListIndex": "94567",
    "statusListCredential": "https://did.actor/alice/credentials/status/3"
  },{
    "id": "https://did.actor/alice/credentials/status/4#23452",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "suspension",
    "statusListIndex": "23452",
    "statusListCredential": "https://did.actor/alice/credentials/status/4"
  }],
  "proof": {
    "type": "JsonWebSignature2020",
    "created": "2023-08-28T13:25:35.827Z",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:did.actor:alice#JWK2020",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Sq5VJHCFuIP-cC86EknRnB91WQxNI5X4QtI0mMR3Xzl8VY5bYfOAtpEsejYeDUbi3Oed0VwQBRCqd11HL7NFF-KH02D9I97nHBftBaXo8e0uWQTRk6TA8xq9oQRuNdnm15eR2zudOzKlH4ArXcBo-hUxUH6EH7YimT0Uu5NbsofN-5C2ovksogbugl-NkW3MKrGkAYzsyaEcgh-vSiRwSl4vwE55sDkn16QgMtsccwo9PR0kzECHp8KQZTM3Nwnv4jNN-F3zP-3Vn0B-cm-UgHPz1RYX6uKrc3A4TlJvk9rxQNfLbNot8ZaQBPoLMnd98bV6giNaGIbekVOUuBxUKg"
  }  
}
```


## Public Party Credential

The **Party Credential** contains data that can be publicly accessed and queried.



# Party Credential Lifecycle

- **expired** if the expiration datetime is older than the current datetime or the certificate containing the key used to sign the claim has expired.
- **revoked** 
  - if the key used to sign the array is revoked.
  - if the **credentialStatus** has the **statusPurpose** property set to **"revocation"** and the value of status at position **credentialIndex** is **true**
- **suspended** if the **credentialStatus** has the **statusPurpose** property set to **"suspension"** and the value of status at position **credentialIndex** is **true**
- **deprecated** if another verifiable credential with the same identifier and the same signature issuer has a newer issuance datetime.
- **active** only if none of the above.

# Party Credential Status 

Verifiable Credentials are a fundamental component of secure data and identity systems, enabling the issuance and presentation of trustworthy and tamper-proof credentials. However, in dynamic and evolving environments, it is crucial to establish mechanisms for the timely revocation or suspension of these credentials in case of compromised or outdated information. 

The use of Credential Status Lists (CSL), specifically the [W3C Verifiable Credentials Bitstring Status List](https://www.w3.org/TR/vc-bitstring-status-list/), addresses this need by providing a standardized approach to manage and communicate the revocation status of verifiable Credentials.


When a Verifiable Credential is issued, the issuer has the option to embed a reference to the Credential Status List (CSL) entry associated with the credential. This reference, often in the form of a Uniform Resource Identifier (URI), enables relying parties (commonly verifiers) to promptly determine the current status of the credential's validity.

To validate the Verifiable Credential, the relying party retrieves the referenced Credential Status List entry using the provided URI. This entry contains information about the status of the credential, allowing to check if the credential is still valid, has been revoked/suspended, or has any other relevant status.

Relying parties can periodically update their local copy of the Credential Status List from trusted sources to ensure they possess the most current revocation status information. This practice prevents reliance on outdated or incorrect information, enhancing the overall security of the ecosystem.

```json linenums="1" title="party_credential_revocation.json" hl_lines="4 11 17"
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://w3id.org/gaia-x/development#"
  ],
  "id": "https://did.actor/alice/credentials/status/3",
  "type": ["VerifiableCredential", "BitstringStatusListCredential"],
  "issuer": "did:web:did.actor:alice",
  "issued": "2021-04-05T14:27:40Z",
  "credentialSubject": {
    "id": "https://example.com/status/3#list",
    "type": "BitstringStatusList",
    "statusPurpose": "revocation",
    "encodedList": "H4sIAAAAAAAAA-3BMQEAAADCoPVPbQwfoAAAAAAAAAAAAAAAAAAAAIC3AYbSVKsAQAAA"
  }
}
```






