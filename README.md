# Identity, Credentials and Access Management

## Access for Gaia-X members

**Please use this [link](https://gitlab.com/groups/gaia-x/-/saml/sso?token=y_xKunAx) to inherit your rights from Gaia-X SSO.**

## Presentation of the Working Group

The objective of the Identity, Credential and Access Management (ICAM) working group is to describe the components for authorisation and authentication which shall deliver core functionalities for authorisation, access management and authentication as well as services around it to Gaia-X Participants with the purpose to join the trustful environment of the Gaia-X Ecosystem. 

## Participants

The list of the members of the WG ICAM is available [here](https://membersplatform.gaia-x.eu/dashboard?locale=en#/members/50) on the Gaia-X Members Platform.

## Meetings schedule

* Every Monday at 5pm CET on Teams - see the [meeting calendar on the Gaia-X members platform](https://membersplatform.gaia-x.eu/dashboard?locale=en#/calendar).

## Specifications

The Identity, Credentials and Access Management document is available at this [page](https://docs.gaia-x.eu)

The work in progress is available [here](https://gaia-x.gitlab.io/technical-committee/identity-credentials-and-access-management-working-group/icam/)


