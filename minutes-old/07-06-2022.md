# Gaia-X TC WG Identity & Access Management - Meeting notes for 07/06/2022

## Minutes

* [proposal] Amjad Ibrahim (Hauwei) : propose a presentation of XACML usage un Huawei on 28th June (link to [this issue](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/13))
* [presentation] Carsten Stoecker resumed the presentation interrupted last week. An issue will be created about root of trust model (action: Carsten).

* [question] We need to clarify who is in charge of vocabulary & schemas definitions: [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/#legal-person) or IAM WG ?