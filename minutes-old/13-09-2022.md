# Gaia-X TC WG Identity & Access Management - Meeting notes for 13/09/2022

## Minutes

We start a new specifications cycle, it's the good time to list participants' wishes and priorities.
An [issues](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/25) has been opened for that.

Discussion about the maturity level of existing components provided by ECO.
The current components aim to be reference implementations that can be used in a production environment. Although they are not all at this level (incomplete documentation, complicated deployment, ...) all contributions to improve them are welcome.

Discussion about the target objective of Gaia-X for Identity and Access management.
The [DID Sig proposal](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/merge_requests/7) aims at trying to define what an authorization management system without a central system (OpenID) could be.


### New issues

* [Issue 25 : Priorities for the next version of the specifications](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/25)
* [Issue 23 : Merge request to architecture document](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/23)


### Still open issues and questions :

* [**Issue 17 : Group leaders election** Please complete for next week](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/17) : See comments in the issue.
* [Issue 6 : Define or get a list of supported resolver on DID](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/6) : Please vote on last comment.
* [Issue 19 : DIDComm use cases](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/19) : Please provide relevant use cases to demonstrate how DIDComm complements OpenID Connect. 

### Closed issues and decisions

* [Issue 18 : Update IDM.AA](https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/18) : We have to update IDM.AA document by end of August (see issue comment), **please make proposals via merge requests**.


