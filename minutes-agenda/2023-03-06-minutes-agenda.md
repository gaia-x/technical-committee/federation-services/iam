# Gaia-X TC WG Identity & Access Management - Meeting notes for 14/02/2023

## Agenda
* Update on compliance, last minutes.
* First merge request from Pierre. Are there any additions we would like to append? 
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/merge_requests/28 
* Input to the architecture document. What do we want to contribute?
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/62
* Discussion on TRAIN and trust concept fragments. 
* Wallet recommendation draft by Yves-Marie
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/blob/develop/WalletRequirements.md
## Minutes

* Discussion of the mr by Pierre. Takeup of naming convention by EBSI is agreed, but we still need to add more content and iterate the changes.
* The adoption of TRAIN and the trust model is causing some debates and disussion. It makes sense to address the issues we have open and take answers from TRAIN as potential contributins, keeping the contribution process safe. 
* Yves-Marie presented the wallet requirements. There is still some work to be done, but this is an excellent starting point. 

### Closed issues and decisions

* .

### Open issues and questions :
* Valid process to integrate content such as the trust process.
* Present a quick rundown of the mission document on the beginning of a Meeting.
  * https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/blob/master/docs/mission_documents/technical-committee_architecture_identity-and-access-management.yaml
* Integration of the trust model still needs to be discussed in a number of issues.

### Discussion and proposals
* We should try to stick to our naming convention for branches. No direct editing of the main branch. 
* We should safeguard our group content and stick to the contribution process. So first issue, then branch and after agreement merge to main. 

### Special note 

