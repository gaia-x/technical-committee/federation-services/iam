# Gaia-X TC WG Identity & Access Management - Meeting notes for 14/02/2023

## Compliance reminder
We adhere to the compliance document of Gaia-X: https://community.gaia-x.eu/apps/files/?dir=/AISBL/Legal%20Documents%20(All)/Code%20of%20Conduct&fileid=14648642
This means, joing the meeting means agreement to these terms:

* No price-fixing.
* No market or customer allocation.
* No output restrictions.
* No agreement on or exchange of competively sensitive business information.

## Agenda
* Compliance reminder.
* Last minutes.
* Input to the architecture document. What do we want to contribute?
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/62
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/52
  * Merge request drafted: https://gitlab.com/gaia-x/technical-committee/architecture-document/-/merge_requests/486
* SWOT analysis
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/54
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/59
* Current status of the TRAIN issue
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/60
* Request regarding issue 61 decision
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/61
* Integrating principals (personal identities)
  * https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/issues/63



## Minutes

* 

### Closed issues and decisions

* 

### Open issues and questions :
* 

### Discussion and proposals
* 

### Special note 

